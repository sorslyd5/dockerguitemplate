# Firefox over VNC
#
# VERSION               0.1
# DOCKER-VERSION        0.2

FROM    ubuntu:14.04
# Make sure the package repository is up to date
RUN     apt-get update

# Install vnc, xvfb in order to create a 'fake' display and firefox
RUN     apt-get install -y x11vnc xvfb
RUN     mkdir ~/.vnc
# Setup a password
RUN     x11vnc -storepasswd 1234 ~/.vnc/passwd

# setup vnc4server (actually seems to work..?)
RUN	apt-get install -y vnc4server

RUN	cd ~ && \
	vnc4server &> display.log && \
	#this is where you use sed magic and grab the display string to auto-config
#	export DISPLAY=$DISP

# setup & run application
#RUN	apt-get install -y firefox
CMD	["python", "app.py"]
